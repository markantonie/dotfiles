
HISTSIZE=10000
SAVEHIST=10000
HISTFILE=$HOME/.cache/zsh/history


autoload -Uz compinit 
compinit -d $HOME/.cache/zsh/compdump



setopt complete_aliases
setopt auto_cd
setopt glob_dots
setopt append_history
setopt inc_append_history
setopt hist_ignore_dups

zstyle ':completion:*' menu select
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'


alias rebuild='sudo nixos-rebuild switch --flake $HOME/.config/nixos#default'

alias yeet='rm -rf'
alias zshrc='nvim $HOME/.zshrc'
alias v='nvim'
alias vz='nvim $HOME/.zshrc'
alias vn='nvim $HOME/.config/nvim/init.vim'
alias hs='hugo serve'
#alias vw='nvim ~/.local/share/vimwiki/index.md'
alias vw='nvim $HOME/storage/shared/Sync/vimwiki/index.md'

alias ..='cd ..'
alias ...='cd ../..'

alias ls='ls --group-directories-first'
alias la='ls -A --group-directories-first'
alias ll='ls -l --group-directories-first'
alias lal='ls -lA --group-directories-first'

############
# dotfiles #
############
alias dotfiles='/usr/bin/git --git-dir=$HOME/.local/share/dotfiles/ --work-tree=$HOME'
alias da='/usr/bin/git --git-dir=$HOME/.local/share/dotfiles/ --work-tree=$HOME add'
alias dc='/usr/bin/git --git-dir=$HOME/.local/share/dotfiles/ --work-tree=$HOME commit'
alias ds='/usr/bin/git --git-dir=$HOME/.local/share/dotfiles/ --work-tree=$HOME status'



fpath+=$HOME/.config/zsh/typewritten
autoload -U promptinit; promptinit
prompt typewritten



#PROMPT='%# '
#autoload -Uz vcs_info
#precmd_vcs_info() { vcs_info }
#precmd_functions+=( precmd_vcs_info )
#setopt prompt_subst
## RPROMPT=\$vcs_info_msg_0_
#RPROMPT=\$vcs_info_msg_0_'%~'
#zstyle ':vcs_info:git:*' formats '%b'


# vim mode config
    # ---------------

    # Activate vim mode.
    bindkey -v

    # Remove mode switching delay.
    KEYTIMEOUT=5

    # Change cursor shape for different vi modes.
    function zle-keymap-select {
      if [[ ${KEYMAP} == vicmd ]] ||
         [[ $1 = 'block' ]]; then
        echo -ne '\e[1 q'

      elif [[ ${KEYMAP} == main ]] ||
           [[ ${KEYMAP} == viins ]] ||
           [[ ${KEYMAP} = '' ]] ||
           [[ $1 = 'beam' ]]; then
        echo -ne '\e[5 q'
      fi
    }
    zle -N zle-keymap-select

    # Use beam shape cursor on startup.
    echo -ne '\e[5 q'

    # Use beam shape cursor for each new prompt.
    preexec() {
       echo -ne '\e[5 q'
    }
#source /data/data/com.termux/files/home/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh


