{
  programs.nixvim.keymaps = [
    {
      mode = "v";
      key = "J";
      action = ":m '>+1<CR>gv=gv";
      options = {
        silent = true;
      };
    }
    {
      mode = "v";
      key = "K";
      action = ":m '<-2<CR>gv=gv";
      options = {
        silent = true;
      };
    }
    {
      # Format file
      key = "<leader>fm";
      action = "<CMD>lua vim.lsp.buf.format()<CR>";
      options.desc = "Format the current buffer";
    }
    {
      action = ":!alejandra -q .<CR>";
      key = "<Leader>mp";
      options = {
        silent = true;
        desc = "Format current file with Alejandra";
      };
    }
    {
      key = "<leader>cf";
      action = "<cmd>lua vim.lsp.buf.format {async = true }<cr>";
      options = {
        silent = true;
        desc = "Format file";
      };
    }
    {
      action = "<cmd>Oil<CR>";
      key = "-";
      options = {
        silent = true;
      };
    }
    # Use tab as buffer switcher in normal mode
    {
      mode = "n";
      key = "<Tab>";
      action = ":bnext<CR>";
      options = {
        silent = true;
      };
    }
    {
      mode = "n";
      key = "<S-Tab>";
      action = ":bprevious<CR>";
      options = {
        silent = true;
      };
    }

    # Delete search highlight with backspace
    {
      mode = "n";
      key = "<BS>";
      action = ":nohlsearch<CR>";
      options = {
        silent = true;
      };
    }

    # Switch between splits more quickly
    {
      mode = "n";
      key = "<C-J>";
      action = "<C-W><C-J>";
    }
    {
      mode = "n";
      key = "<C-K>";
      action = "<C-W><C-K>";
    }
    {
      mode = "n";
      key = "<C-L>";
      action = "<C-W><C-L>";
    }
    {
      mode = "n";
      key = "<C-H>";
      action = "<C-W><C-H>";
    }
  ];
}
