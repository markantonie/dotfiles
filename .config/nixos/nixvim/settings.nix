{
  programs.nixvim = {
    globals.mapleader = " ";
    clipboard.providers.xclip.enable = true;
    opts = {
      number = true;
      relativenumber = true;

      tabstop = 2;
      shiftwidth = 2;
      expandtab = true;

      smartindent = true;
      smartcase = true;

      termguicolors = true;
      hlsearch = true;
      incsearch = true;

      timeoutlen = 350;
    };
  };
}
