{
  programs.nixvim.plugins = {
    alpha = {
      enable = true;
      # theme = "theta"; # doesn't work
      # theme = "dashboard";
      theme = "startify";
    };
  };
}
