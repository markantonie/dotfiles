{inputs, ...}: {
  imports = [
    inputs.nixvim.homeManagerModules.nixvim

    ./keymaps.nix
    ./settings.nix

    ./alpha.nix
    # ./bufferline.nix
    ./colorscheme.nix
    ./comment.nix
    ./whichkey.nix
    ./treesitter.nix
    ./telescope.nix
    ./lsp.nix
    ./oil.nix
    ./indent-blankline.nix
    # ./noice.nix
    # ./undotree.nix
    ./conform.nix
    ./completion.nix
    # ./hardtime.nix
    ./flash.nix
  ];

  programs.nixvim = {
    enable = true;
  };
}
