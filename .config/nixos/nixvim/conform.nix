{
  programs.nixvim.plugins.conform-nvim = {
    enable = true;
    # formatOnSave = {
    #   lspFallback = true;
    #   timeoutMs = 500;
    # };
    formattersByFt = {
      nix = ["alejandra" "nixfmt" "nixpkgs_fmt"];
      lua = ["stylua"];
      markdown = ["prettierd" "prettier"];
    };
  };
}
