{
  programs.nixvim.plugins.telescope = {
    enable = true;
    keymaps = {
      "<C-p>" = {
        action = "git_files";
        options = {
          desc = "[G]it Files";
        };
      };
      "<leader>fg" = {
        action = "live_grep";
        options = {
          desc = "[G]rep";
        };
      };
      "<leader>ff" = {
        action = "find_files";
        options = {
          desc = "[F]iles";
        };
      };
      "<leader>fb" = {
        action = "buffers";
        options = {
          desc = "[B]uffers";
        };
      };
      "<leader>fh" = {
        action = "oldfiles";
        options = {
          desc = "[H]istory";
        };
      };
      "<leader>vh" = {
        action = "help_tags";
        options.desc = "[H]elp tags";
      };
    };
  };
}
