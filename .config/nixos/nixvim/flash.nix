{
  programs.nixvim = {
    plugins.flash = {
      enable = true;
      label = {
        rainbow.enabled = false;
      };
      highlight.groups.label = "FlashCurrent";
      modes.char = {
        enabled = true;
        jumpLabels = true;
      };
    };
    keymaps = [
      {
        action = "<cmd>lua require('flash').jump() <cr>";
        mode = ["n" "x" "o"];
        key = "s";
        options = {
          desc = "Flash";
        };
      }
      {
        action = "<cmd>lua require('flash').treesitter() <cr>";
        mode = ["n" "x" "o"];
        key = "S";
        options = {
          desc = "Flash Treesitter";
        };
      }
      {
        action = "<cmd>lua require('flash').remote()<cr>";
        mode = "o";
        key = "r";
        options = {
          desc = "Remote Flash";
        };
      }
      {
        action = "<cmd>lua require('flash').treesitter_search()<cr>";
        mode = ["o" "x"];
        key = "R";
        options = {
          desc = "treesitter_search";
        };
      }
      {
        action = "<cmd>lua require('flash').toggle()<cr>";
        mode = "c";
        key = "<C-s>";
        options.desc = "Toggle Flash Search";
      }
      #     { "<c-s>", mode = { "c" }, function() require("flash").toggle() end, desc = "Toggle Flash Search" },
    ];
  };
}
# {
#   "folke/flash.nvim",
#   event = "VeryLazy",
#   ---@type Flash.Config
#   opts = {},
#   -- stylua: ignore
#   keys = {
#     { "s", mode = { "n", "x", "o" }, function() require("flash").jump() end, desc = "Flash" },
#     { "S", MODE = { "n", "x", "o" }, function() require("flash").treesitter() end, desc = "Flash Treesitter" },
#     { "r", mode = "o", function() require("flash").remote() end, desc = "Remote Flash" },
#     { "R", mode = { "o", "x" }, function() require("flash").treesitter_search() end, desc = "Treesitter Search" },
#     { "<c-s>", mode = { "c" }, function() require("flash").toggle() end, desc = "Toggle Flash Search" },
#   },
# }

