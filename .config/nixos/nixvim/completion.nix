{pkgs, ...}: {
  programs.nixvim.plugins = {
    luasnip = {
      enable = true;
      fromVscode = [
        {paths = "${pkgs.vimPlugins.friendly-snippets}";}
      ];
    };
    cmp-buffer.enable = true;
    cmp-nvim-lsp.enable = true;
    cmp-path.enable = true;
    cmp_luasnip.enable = true;
    cmp = {
      enable = true;
      settings = {
        # mapping = {
        #   __raw = ''
        #     cmp.mapping.preset.insert({
        #       ['<C-b>'] = cmp.mapping.scroll_docs(-4),
        #       ['<C-f>'] = cmp.mapping.scroll_docs(4),
        #       ['<C-Space>'] = cmp.mapping.complete(),
        #       ['<C-e>'] = cmp.mapping.abort(),
        #       ['<CR>'] = cmp.mapping.confirm({ select = true }),
        #     })
        #   '';
        # };
        mapping = {
          "<C-n>" = "cmp.mapping.select_next_item()";
          "<C-p>" = "cmp.mapping.select_prev_item()";
          "<C-j>" = "cmp.mapping.select_next_item()";
          "<C-k>" = "cmp.mapping.select_prev_item()";
          "<C-d>" = "cmp.mapping.scroll_docs(-4)";
          "<C-f>" = "cmp.mapping.scroll_docs(4)";
          "<C-Space>" = "cmp.mapping.complete()";
          "<C-e>" = "cmp.mapping.close()";
          "<CR>" = "cmp.mapping.confirm({ behavior = cmp.ConfirmBehavior.Insert, select = true })";
        };
        snippet = {
          expand = "function(args) 
            require('luasnip').lsp_expand(args.body) 
          end";
        };
        sources = [
          {name = "nvim_lsp";}
          {name = "luasnip";}
          {
            name = "buffer";
            option.get_bufnrs.__raw = "vim.api.nvim_list_bufs";
          }
          {name = "path";}
          {name = "emoji";}
        ];
      };
    };
  };
}
