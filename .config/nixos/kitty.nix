{
  programs.kitty = {
    enable = true;
    theme = "Catppuccin-Mocha";
    font = {
      name = "3270 Nerd Font";
      # package = pkgs.inconsolata-nerdfont;
      size = 14;
    };
    settings = {
      enable_audio_bell = false;
      confirm_os_window_close = -0;
      copy_on_select = true;
      clipboard_control = "write-clipboard read-clipboard write-primary read-primary";
    };
  };
}
