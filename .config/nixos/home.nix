{
  pkgs,
  inputs,
  username,
  ...
}: {
  imports = [
    # ./nixvim
    ./zsh
    ./kitty.nix
  ];

  # Home Manager needs a bit of information about you and the paths it should
  # manage.
  # home.username = "mark";
  home.username = "${username}";
  home.homeDirectory = "/home/mark";

  nixpkgs.config.allowUnFree = true;

  programs = {
    neovim = {
      enable = true;
    };
    git = {
      enable = true;
      userName = "Mark Antonie";
      userEmail = "markantoniedh@gmail.com";
    };
    firefox = {
      enable = true;
      profiles = {
        test = {
          id = 0;
          name = "default";
          isDefault = true;
          settings = {
            # from about:config
            "browser.newtabpage.activity-stream.improvesearch.topSiteSearchShortcuts" = "false";
            "browser.startup.couldRestoreSession.count" = "2";
            # "browser.startup.homepage" = "about:blank";
            "browser.urlbar.placeholderName" = "1234567890";
            "dom.security.https_only_mode" = true;
            "extensions.activeThemeID" = "firefox-compact-dark@mozilla.org";
            "browser.translations.enable" = false;
            "extensions.pocket.enabled" = false;
          };
          search.force = true;
          search.engines = {
            "Nix Packages" = {
              urls = [
                {
                  # template = "https://search.nixos.org/packages";
                  template = "https://search.nixos.org/packages?channel=unstable&from=0&size=50&sort=relevance&type=packages&query=";
                  params = [
                    {
                      name = "type";
                      value = "packages";
                    }
                    {
                      name = "query";
                      value = "{searchTerms}";
                    }
                  ];
                }
              ];
              definedAliases = ["@np"];
            };
            "Arch Wiki" = {
              urls = [
                {
                  template = "https://wiki.archlinux.org/title";
                  params = [
                    {
                      name = "type";
                      value = "packages";
                    }
                    {
                      name = "query";
                      value = "{searchTerms}";
                    }
                  ];
                }
              ];
              definedAliases = ["@aw"];
            };
          };

          extensions = with inputs.firefox-addons.packages."x86_64-linux"; [
            # https://gitlab.com/rycee/nur-expressions/-/blob/master/pkgs/firefox-addons/addons.json
            ublock-origin
            # tridactyl
            vimium
            # consent-o-matic
          ];
        };
      };
    };

    bat.enable = true;
    fzf.enable = true;
    fzf.enableZshIntegration = true;

    # kitty = {
    #   enable = true;
    #   theme = "Catppuccin-Mocha";
    #   font = {
    #     name = "3270 Nerd Font";
    #     # package = pkgs.inconsolata-nerdfont;
    #     size = 14;
    #   };
    #   settings = {
    #     enable_audio_bell = false;
    #     confirm_os_window_close = -0;
    #     copy_on_select = true;
    #     clipboard_control = "write-clipboard read-clipboard write-primary read-primary";
    #   };
    # };


    # xmobar = {
    #   enable = true;
    #   package = pkgs.haskellPackages.xmobar;
    # };
  };

  services.dunst = {
    enable = true;
  };

  # This value determines the Home Manager release that your configuration is
  # compatible with. This helps avoid breakage when a new Home Manager release
  # introduces backwards incompatible changes.
  #
  # You should not change this value, even if you update Home Manager. If you do
  # want to update the value, then make sure to first check the Home Manager
  # release notes.
  home.stateVersion = "23.11"; # Please read the comment before changing.

  # The home.packages option allows you to install Nix packages into your
  # environment.
  home.packages = with pkgs; [
    # # Adds the 'hello' command to your environment. It prints a friendly
    # # "Hello, world!" when run.
    # pkgs.hello

    # # It is sometimes useful to fine-tune packages, for example, by applying
    # # overrides. You can do that directly here, just don't forget the
    # # parentheses. Maybe you want to install Nerd Fonts with a limited number of
    # # fonts?
    # (pkgs.nerdfonts.override { fonts = [ "FantasqueSansMono" ]; })

    # # You can also create simple shell scripts directly inside your
    # # configuration. For example, this adds a command 'my-hello' to your
    # # environment:
    # (pkgs.writeShellScriptBin "my-hello" ''
    #   echo "Hello, ${config.home.username}!"
    # '')
    htop
    btop
    brave
    dmenu
    gcc
    (nerdfonts.override {fonts = ["3270" "InconsolataGo" "Arimo"];})
    ripgrep
    tree
    xclip
    ghc
    tldr
    nixpkgs-fmt
    nixfmt-classic
    alacritty
  ];

  fonts.fontconfig.enable = true;

  # Home Manager is pretty good at managing dotfiles. The primary way to manage
  # plain files is through 'home.file'.
  home.file = {
    # # Building this configuration will create a copy of 'dotfiles/screenrc' in
    # # the Nix store. Activating the configuration will then make '~/.screenrc' a
    # # symlink to the Nix store copy.
    # ".screenrc".source = dotfiles/screenrc;

    # # You can also set the file content immediately.
    # ".gradle/gradle.properties".text = ''
    #   org.gradle.console=verbose
    #   org.gradle.daemon.idletimeout=3600000
    # '';
  };

  # Home Manager can also manage your environment variables through
  # 'home.sessionVariables'. If you don't want to manage your shell through Home
  # Manager then you have to manually source 'hm-session-vars.sh' located at
  # either
  #
  #  ~/.nix-profile/etc/profile.d/hm-session-vars.sh
  #
  # or
  #
  #  ~/.local/state/nix/profiles/profile/etc/profile.d/hm-session-vars.sh
  #
  # or
  #
  #  /etc/profiles/per-user/mark/etc/profile.d/hm-session-vars.sh
  #

  home.sessionVariables = {
    EDITOR = "nvim";
    ZDOTDIR = "$HOME/config/zsh";
  };

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
}
