{
  imports = [
    ./aliases.nix
    ./starship.nix
  ];
  programs.zsh = {
    enable = true;
    dotDir = ".config/zsh/";
    defaultKeymap = "viins";
    autosuggestion.enable = true;
    syntaxHighlighting.enable = true;
    autocd = true;
    cdpath = [
      "$HOME"
      "$HOME/.config/"
      "$HOME/.config/nixos"
    ];
    history = {
      size = 10000;
      path = "$ZDOTDIR/history";
      ignoreSpace = true;
      ignoreDups = true;
    };
  };
}
