{
  programs.zsh.shellAliases = {
    v = "nvim";
    vc = "nvim $HOME/.config/nixos/configuration.nix";
    vf = "nvim $HOME/.config/nixos/flake.nix";
    vh = "nvim $HOME/.config/nixos/home.nix";

    vx = "nvim $HOME/.config/xmonad/xmonad.hs";
    vb = "nvim $HOME/.config/xmobar/xmobar.hs";
    yeet = "rm -rf";

    rebuild = "sudo nixos-rebuild switch --flake $HOME/.config/nixos#";

    l = "ls --group-directories-first";
    la = "ls -A --group-directories-first";
    ll = "ls -l --group-directories-first";
    lal = "ls -Al --group-directories-first";
  };
}
