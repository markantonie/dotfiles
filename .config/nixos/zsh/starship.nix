{lib, ...}: {
  programs.starship = {
    enable = true;
    # Configuration written to ~/.config/starship.toml
    settings = {
      # settings = pkgs.lib.importTOML ../starship.toml;
      add_newline = false;
      format = "$character";
      right_format = lib.concatStrings [
        "$directory"
        "$git_branch"
        "$git_commit"
        "$git_state"
        "$git_metrics"
        "$git_status"
        "$status"
        "$haskell"
        "$lua"
        "$nixshell"
        "$sudo"
      ];
    };
  };
}
