return {
	"nvimtools/none-ls.nvim",
	dependencies = "nvim-lua/plenary.nvim",
	config = function()
		local null_ls = require("null-ls")

		null_ls.setup({
			sources = {
				null_ls.builtins.formatting.stylua,   --lua
				null_ls.builtins.formatting.markdownlint,
                null_ls.builtins.formatting.prettierd,
				-- null_ls.builtins.formatting.fourmolu, --haskell
				null_ls.builtins.diagnostics.selene,  --lua
				null_ls.builtins.diagnostics.markdownlint,
				--                null_ls.builtins.formatting.nixpkgs-fmt,
				null_ls.builtins.completion.spell,
			},
		})
	end,
}
