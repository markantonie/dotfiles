local o = vim.opt

o.number         = true
o.relativenumber = true

o.tabstop        = 2
o.shiftwidth     = 2
o.expandtab      = true

o.smartindent    = true
o.smartcase      = true

o.termguicolors  = true

o.hlsearch = false
o.incsearch = true

-- o.scrolloff = 8
-- o.colorcolumn = "80"
