vim.g.mapleader = " "

vim.keymap.set("n", "<leader>L", ":Lazy<CR>", { desc = "Lazy" })
vim.keymap.set("n", "<leader>M", ":Mason<CR>", { desc = "Mason" })

--vim.keymap.set("n", "<leader>pv", ":Ex<CR>", { desc = "Netrw" })
vim.keymap.set("n", "<leader>pv", vim.cmd.Ex, { desc = "Netrw" })

vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")

vim.keymap.set("n", "J", "mzJ`z")
vim.keymap.set("n", "<C-d>", "<C-d>zz")
vim.keymap.set("n", "<C-u>", "<C-u>zz")
vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")

-- greatest remap ever
vim.keymap.set("x", "<leader>p", [["_dP]])

-- next greatest remap ever : asbjornHaland
-- Copy text to system clipboard with <leader>y.
vim.keymap.set({ "n", "v" }, "<leader>y", [["+y]])
vim.keymap.set("n", "<leader>Y", [["+Y]])

vim.keymap.set("n", "<leader><leader>", function()
	vim.cmd("so")
end)

vim.keymap.set("n", "<C-h>", "<C-w>h", { desc = "Left" })
vim.keymap.set("n", "<C-j>", "<C-w>j", { desc = "Down" })
vim.keymap.set("n", "<C-k>", "<C-w>k", { desc = "Up" })
vim.keymap.set("n", "<C-l>", "<C-w>l", { desc = "Right" })
